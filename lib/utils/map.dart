import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';

const CIRCLE_ID = 'CIRCLE_ID';

Future<LatLng> getUserLocation() async {
  Location location = Location();

  bool _serviceEnabled;
  PermissionStatus _permissionGranted;

  _serviceEnabled = await location.serviceEnabled();
  if (!_serviceEnabled) {
    _serviceEnabled = await location.requestService();
    if (!_serviceEnabled) {
      return null;
    }
  }

  _permissionGranted = await location.hasPermission();
  if (_permissionGranted == PermissionStatus.denied) {
    _permissionGranted = await location.requestPermission();
    if (_permissionGranted != PermissionStatus.granted) {
      return null;
    }
  }

  final locationData = await location.getLocation();
  return LatLng(locationData.latitude, locationData.longitude);
}

// https://stackoverflow.com/questions/54211577/how-to-create-circle-on-current-location-in-flutter
Set<Circle> getCircle(LatLng userLocation, double radius) {
  final Set<Circle> circle = Set.from([
    Circle(
      circleId: CircleId(CIRCLE_ID),
      fillColor: Color(0x7F2196F3),
      strokeWidth: 0,
      center: userLocation,
      radius: radius,
    )
  ]);
  return circle;
}

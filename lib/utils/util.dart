import 'dart:math';

import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:setel_task/models/wifi.dart';

import '../models/device.dart';

// https://github.com/Baseflow/flutter-geolocator/issues/23
// not really sure what those values are other then 'p',
// might take a look at what the actual formula is later
double calculateDistance(LatLng start, LatLng end) {
  var p = pi / 180;
  var c = cos;
  var a = 0.5 -
      c((end.latitude - start.latitude) * p) / 2 +
      c(start.latitude * p) *
          c(end.latitude * p) *
          (1 - c((end.longitude - start.longitude) * p)) /
          2;
  final result = 12742 * asin(sqrt(a));
  return num.parse(result.toStringAsFixed(2))*1000;
}

bool isDeviceInsideWifiRadius(Device device, Wifi wifi) {
  try {
    final distance = calculateDistance(device?.location, wifi?.location);
    return distance <= wifi.radius;
  } catch (_) {
    return false;
  }
}

bool isDeviceConnectedToWifi(Device device, Wifi wifi) {
  try {
    return device?.wifi == wifi?.id;
  } catch (_) {
    return false;
  }
}

bool isDeviceUnderWifiCoverage(Device device, Wifi wifi) =>
    isDeviceInsideWifiRadius(device, wifi) ||
    isDeviceConnectedToWifi(device, wifi);

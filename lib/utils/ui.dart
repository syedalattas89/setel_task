import 'package:flutter/material.dart';

FlatButton generalFlatButton({
  String title,
  Function callBack,
}) =>
    FlatButton(
      child: Text(title),
      onPressed: callBack,
      color: Colors.blue,
      textColor: Colors.white,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(100),
      ),
    );

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:setel_task/bloc/main/main_bloc.dart';
import 'package:setel_task/bloc/main/main_event.dart';
import 'package:setel_task/bloc/map/map_bloc.dart';
import 'package:setel_task/bloc/map/map_state.dart';
import 'package:setel_task/localizations.dart';
import 'package:setel_task/utils/map.dart';
import 'package:setel_task/utils/ui.dart';
import 'package:uuid/uuid.dart';

import 'bloc/map/map_event.dart';
import 'models/wifi.dart';

const MARKER_WIFI_ID = 'MARKER_WIFI';
const MARKER_DEVICE_ID = 'MARKER_DEVICE';

class WifiSetting extends StatefulWidget {
  final Wifi wifi;

  const WifiSetting({Key key, this.wifi}) : super(key: key);

  @override
  _WifiSettingState createState() => _WifiSettingState();
}

class _WifiSettingState extends State<WifiSetting> {
  @override
  Widget build(BuildContext context) {
    AppLocalizations _locale = AppLocalizations.of(context);

    // init map stuff
    Completer<GoogleMapController> _controller = Completer();
    GoogleMapController controller;
    CameraPosition _initialPosition;

    // init map ui
    Map<MarkerId, Marker> markers = <MarkerId, Marker>{};
    Set<Circle> _circles;

    // ui
    final double padding = 16;
    final TextEditingController wifiNameController = TextEditingController();
    ValueNotifier<bool> isWifiNameEmpty = ValueNotifier<bool>(false);

    // init bloc
    final MainBloc mainBloc = BlocProvider.of<MainBloc>(context);
    final MapBloc mapBloc = BlocProvider.of<MapBloc>(context);

    // device location
    final LatLng deviceLocation = mainBloc.state.device.location;

    // check if wifi to add or to update
    Wifi wifiToUpdate = widget.wifi;
    if (wifiToUpdate != null) {
      // set initial value using existing wifi information
      wifiNameController.text = wifiToUpdate.name;
      mapBloc.add(AddInitialMarker(wifiLocation: wifiToUpdate.location));
    } else {
      // set initial value using device location if wifi information null
      mapBloc.add(AddInitialMarker(wifiLocation: deviceLocation));
    }

    // animate to device location when location is ready
    Future<void> _animateCameraToActualLocation(LatLng actualLocation) async {
      controller = await _controller.future;
      CameraPosition _actualCameraLocation = CameraPosition(
        target: actualLocation,
        zoom: 15,
      );
      controller
          .animateCamera(CameraUpdate.newCameraPosition(_actualCameraLocation));
    }

    // update wifi info
    _updateWifi(LatLng wifiLocation) {
      isWifiNameEmpty.value = wifiNameController.value.text.isEmpty;
      if (!isWifiNameEmpty.value) {
        final Wifi wifi = Wifi(
          id: wifiToUpdate?.id ?? Uuid().v1(),
          radius: 500,
          location: wifiLocation,
          name: wifiNameController.value.text,
        );

        mainBloc.add(UpdateWifiList(wifi));
        Navigator.pop(context);
      }
    }

    return Scaffold(
      appBar: AppBar(
        title: Text(_locale.setWiFi),
      ),
      body: BlocBuilder<MapBloc, MapState>(
        builder: (BuildContext context, MapState state) {
          // move the camera to device location when it is available
          if (state.isLocationUpdated) {
            _animateCameraToActualLocation(deviceLocation);
          }

          // set camera initial position
          _initialPosition = CameraPosition(
            target: deviceLocation,
            zoom: 15,
          );

          // get the circle for map location
          _circles = getCircle(state.markerLocation, 500);

          // set map marker
          final Marker wifiMarker = Marker(
            markerId: MarkerId(MARKER_WIFI_ID),
            // set the marker slightly off device marker
            anchor: Offset(0.75, 1.0),
            draggable: true,
            position: state.markerLocation,
            onDragEnd: (newPosition) {
              mapBloc.add(UpdateMarkerLocation(newPosition));
            },
          );

          // set device marker
          final Marker deviceMarker = Marker(
            markerId: MarkerId(MARKER_DEVICE_ID),
            draggable: false,
            icon:
                BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueBlue),
            position: deviceLocation,
          );

          // display some hint on what to do
          Widget _guideToUpdateLocation() => RichText(
                text: TextSpan(
                  children: [
                    TextSpan(
                      text: _locale.drag,
                      style: TextStyle(color: Colors.grey),
                    ),
                    TextSpan(
                      text: _locale.marker,
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.red,
                      ),
                    ),
                    TextSpan(
                        text: _locale.toAdjustWifiLocation,
                        style: TextStyle(color: Colors.grey)),
                  ],
                ),
              );

          // display some hint on what to do
          Widget _guideDeviceLocation() => RichText(
                text: TextSpan(
                  children: [
                    TextSpan(
                      text: _locale.marker,
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.blue,
                      ),
                    ),
                    TextSpan(
                        text: _locale.isDeviceLocation,
                        style: TextStyle(color: Colors.grey)),
                  ],
                ),
              );

          // assign marker to the markers hashmap
          // will use later to differentiate the device marker
          markers = {
            deviceMarker.markerId: deviceMarker,
            wifiMarker.markerId: wifiMarker,
          };

          return Stack(
            children: <Widget>[
              GoogleMap(
                initialCameraPosition: _initialPosition,
                markers: Set.from(markers.values),
                circles: _circles,
                onMapCreated: (GoogleMapController controller) {
                  _controller.complete(controller);
                },
              ),
              Positioned(
                bottom: 0,
                right: padding,
                left: padding,
                child: SafeArea(
                  child: Card(
                    elevation: 10,
                    shadowColor: Colors.grey[50],
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Padding(
                      padding: EdgeInsets.all(padding),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: <Widget>[
                          _guideToUpdateLocation(),
                          _guideDeviceLocation(),
                          Text(
                            _locale.wifiRadiusIsSet,
                            style: TextStyle(color: Colors.grey),
                          ),
                          Padding(padding: EdgeInsets.all(8)),
                          ValueListenableBuilder(
                            valueListenable: isWifiNameEmpty,
                            builder: (BuildContext context, bool value,
                                Widget child) {
                              return TextField(
                                controller: wifiNameController,
                                decoration: InputDecoration(
                                    hintText: _locale.insertWifiNameHere,
                                    errorText: value
                                        ? _locale.pleaseInsertName
                                        : null),
                              );
                            },
                          ),
                          Padding(padding: EdgeInsets.all(8)),
                          generalFlatButton(
                            title: _locale.saveWifi,
                            callBack: () => _updateWifi(state.markerLocation),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ],
          );
        },
      ),
    );
  }
}

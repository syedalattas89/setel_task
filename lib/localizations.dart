import 'dart:async';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import 'l10n/messages_all.dart';

class AppLocalizations {
  static Future<AppLocalizations> load(Locale locale) {
    final String name =
        locale.countryCode == null ? locale.languageCode : locale.toString();
    final String localeName = Intl.canonicalizedLocale(name);

    return initializeMessages(localeName).then((bool _) {
      Intl.defaultLocale = localeName;
      return new AppLocalizations();
    });
  }

  static AppLocalizations of(BuildContext context) {
    return Localizations.of<AppLocalizations>(context, AppLocalizations);
  }

  final String setelTask = Intl.message('Setel Task');
  final String deviceInformation = Intl.message('DEVICE INFORMATION:');
  final String latitude = Intl.message('Latitude:');
  final String longitude = Intl.message('Longitude:');
  final String iWantToSetWifiNow = Intl.message('I want to set wifi now');
  final String addWifi = Intl.message('Add Wifi');
  final String connected = Intl.message('Connected');
  final String notConnected = Intl.message('Not Connected');
  final String noWifiDetected = Intl.message('No wifi detected');
  final String noData = Intl.message('No Data');
  final String setWiFi = Intl.message('Set WiFi');
  final String drag = Intl.message('Drag');
  final String marker = Intl.message(' MARKER ');
  final String toAdjustWifiLocation = Intl.message('to adjust wifi location');
  final String isDeviceLocation = Intl.message(' is device location (static)');
  final String insertWifiNameHere = Intl.message('Insert wifi name here');
  final String saveWifi = Intl.message('Save WiFi');
  final String pleaseInsertName = Intl.message('Please insert name');
  final String tapToConnect =
      Intl.message('*tap to connect wifi\n*hold to edit wifi');
  final String wifiRadiusIsSet =
      Intl.message('*Wifi radius is set to 500 meter');

  String distanceValue(double value) => Intl.message('Distance: $value');

  String insideWifiMeterRadius(bool value) =>
      Intl.message('Inside wifi 500 meter radius: $value');

  String geofenceStatus(bool value) => Intl.message('Geofence status: $value');

  String get title {
    return Intl.message('Hello world App',
        name: 'title', desc: 'The application title');
  }

  String get hello {
    return Intl.message('Hello', name: 'hello');
  }
}

class AppLocalizationsDelegate extends LocalizationsDelegate<AppLocalizations> {
  const AppLocalizationsDelegate();

  @override
  bool isSupported(Locale locale) {
    return ['en', 'es', 'pt'].contains(locale.languageCode);
  }

  @override
  Future<AppLocalizations> load(Locale locale) {
    return AppLocalizations.load(locale);
  }

  @override
  bool shouldReload(LocalizationsDelegate<AppLocalizations> old) {
    return false;
  }
}

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:setel_task/bloc/main/main_event.dart';
import 'package:setel_task/bloc/main/main_state.dart';
import 'package:setel_task/models/device.dart';
import 'package:setel_task/models/wifi.dart';
import 'package:setel_task/utils/map.dart';

class MainBloc extends Bloc<MainEvent, MainState> {
  @override
  MainState get initialState => MainState.initial();

  @override
  Stream<MainState> mapEventToState(MainEvent event) async* {
    if (event is AddDevice) {
      final LatLng location = await getUserLocation();
      if (location != null) {
        final Device device = Device(
          location: location,
          wifi: null,
        );
        yield state.copyWith(device: device);
      }
    }

    if (event is SetActiveWifi) {
      yield state.copyWith(
          activeWifi: event.wifi,
          device: state.device.copyWith(wifi: event.wifi.id));
    }

    if (event is UpdateWifiList) {
      Wifi wifi = event.wifi;
      Map<String, Wifi> wifiList = state.wifiList;
      yield state.copyWith(
          wifiList: Map<String, Wifi>.from(wifiList)
            ..update(
              wifi.id,
              (value) => Wifi(
                id: wifi.id,
                name: wifi.name,
                location: wifi.location,
                radius: wifi.radius,
              ),
              ifAbsent: () => wifi,
            ));
    }
  }
}

import 'package:equatable/equatable.dart';
import 'package:setel_task/models/device.dart';
import 'package:setel_task/models/wifi.dart';

class MainState extends Equatable {
  final Device device;
  final Map<String, Wifi> wifiList;
  final Wifi activeWifi;

  MainState({
    this.device,
    this.wifiList,
    this.activeWifi,
  });

  MainState copyWith({
    Device device,
    Map<String, Wifi> wifiList,
    Wifi activeWifi,
  }) =>
      MainState(
        device: device ?? this.device,
        wifiList: wifiList ?? this.wifiList,
        activeWifi: activeWifi ?? this.activeWifi,
      );

  factory MainState.initial() => MainState(
        device: null,
        wifiList: {},
        activeWifi: null,
      );

  @override
  List<Object> get props => [
        device,
        wifiList,
        activeWifi,
      ];
}

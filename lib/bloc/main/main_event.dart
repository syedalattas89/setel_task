import 'package:equatable/equatable.dart';
import 'package:setel_task/models/wifi.dart';

abstract class MainEvent extends Equatable {
  const MainEvent();

  @override
  bool get stringify => true;

  @override
  List<Object> get props => [];
}

class AddDevice extends MainEvent {}

class SetActiveWifi extends MainEvent {
  final Wifi wifi;

  SetActiveWifi(this.wifi);

  @override
  List<Object> get props => [wifi];
}


class UpdateWifiList extends MainEvent {
  final Wifi wifi;

  UpdateWifiList(this.wifi);

  @override
  List<Object> get props => [wifi];
}

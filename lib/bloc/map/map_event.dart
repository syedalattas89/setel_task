import 'package:equatable/equatable.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

abstract class MapEvent extends Equatable {
  const MapEvent();

  @override
  bool get stringify => true;

  @override
  List<Object> get props => [];
}

class AddInitialMarker extends MapEvent {
  final LatLng wifiLocation;

  AddInitialMarker({this.wifiLocation});

  @override
  List<Object> get props => [wifiLocation];
}

class UpdateMarkerLocation extends MapEvent {
  final LatLng location;

  UpdateMarkerLocation(this.location);

  @override
  List<Object> get props => [location];
}

import 'package:flutter_bloc/flutter_bloc.dart';

import 'map_event.dart';
import 'map_state.dart';

class MapBloc extends Bloc<MapEvent, MapState> {
  @override
  MapState get initialState => MapState.initial();

  @override
  Stream<MapState> mapEventToState(MapEvent event) async* {
    if (event is AddInitialMarker) {
      // reset condition
      yield state.copyWith(isLocationUpdated: false);

      yield state.copyWith(
          isLocationUpdated: true,
          markerLocation: event.wifiLocation,
          );
    }

    if (event is UpdateMarkerLocation) {
      yield state.copyWith(markerLocation: event.location);
    }

  }
}

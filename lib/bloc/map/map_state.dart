import 'package:equatable/equatable.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class MapState extends Equatable {
  final bool isLocationUpdated;
  final LatLng markerLocation;

  const MapState({
    this.isLocationUpdated,
    this.markerLocation,
  });

  MapState copyWith({
    bool isLocationUpdated,
    LatLng markerLocation,
  }) =>
      MapState(
        isLocationUpdated: isLocationUpdated ?? this.isLocationUpdated,
        markerLocation: markerLocation ?? this.markerLocation,
      );

  factory MapState.initial() {
    LatLng initialLocation = LatLng(37.42796133580664, -122.085749655962);
    return MapState(
      isLocationUpdated: false,
      markerLocation: initialLocation,
    );
  }

  @override
  List<Object> get props => [
        isLocationUpdated,
        markerLocation,
      ];

  @override
  bool get stringify => true;
}

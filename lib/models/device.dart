import 'package:flutter/cupertino.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class Device {
  final LatLng location;
  final String wifi;

  Device({
    @required this.location,
    @required this.wifi,
  });

  Device copyWith({
    LatLng location,
    String wifi,
  }) =>
      Device(
        location: location ?? this.location,
        wifi: wifi ?? this.wifi,
      );
}

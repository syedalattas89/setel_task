import 'package:flutter/cupertino.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class Wifi {
  final String id;
  final String name;
  final LatLng location; // geographic point
  final int radius;
  final bool isConnectWithDevice;

  Wifi({
    @required this.id,
    @required this.name,
    @required this.location,
    @required this.radius,
    this.isConnectWithDevice = false,
  });
}

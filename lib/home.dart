import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:setel_task/bloc/main/main_bloc.dart';
import 'package:setel_task/bloc/main/main_event.dart';
import 'package:setel_task/bloc/main/main_state.dart';
import 'package:setel_task/localizations.dart';
import 'package:setel_task/utils/util.dart';

import 'models/device.dart';
import 'models/wifi.dart';
import 'utils/ui.dart';
import 'wifi_setting.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    final AppLocalizations _locale = AppLocalizations.of(context);
    BlocProvider.of<MainBloc>(context).add(AddDevice());

    return Scaffold(
      appBar: AppBar(
        title: Text(_locale.setelTask),
      ),
      body: BlocBuilder<MainBloc, MainState>(
        builder: (BuildContext context, MainState state) {
          Device device = state?.device;

          _navigateToWifiSettings(Wifi wifi) {
            if (device != null) {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => WifiSetting(wifi: wifi),
                ),
              );
            } else {
              // request for permission if not yet
              BlocProvider.of<MainBloc>(context).add(AddDevice());
            }
          }

          FlatButton _buttonNavigateToWifiSetup() {
            String title = state.wifiList.isNotEmpty
                ? _locale.addWifi
                : _locale.iWantToSetWifiNow;

            return generalFlatButton(
              title: title,
              callBack: () => _navigateToWifiSettings(null),
            );
          }

          Widget _displayDeviceInformation() => Column(
                children: <Widget>[
                  Text(_locale.deviceInformation),
                  Text('${_locale.latitude} '
                      '${device?.location?.latitude ?? _locale.noData}'),
                  Text('${_locale.longitude} '
                      '${device?.location?.longitude ?? _locale.noData}'),
                ],
              );

          _setAsActiveWifi(Wifi wifi) {
            BlocProvider.of<MainBloc>(context).add(SetActiveWifi(wifi));
          }

          Text _trueOrFalseText(String value, bool isTrue) => Text(
                value,
                style: TextStyle(color: isTrue ? Colors.blue : Colors.red),
              );

          Widget _cardWifi(Wifi wifi) {
            final double totalDistance =
                calculateDistance(device.location, wifi.location);

            final bool isConnectedToWifi =
                isDeviceConnectedToWifi(device, wifi);

            final bool isInsideRadius = isDeviceInsideWifiRadius(device, wifi);

            final bool isUnderWifiCoverage =
                isDeviceUnderWifiCoverage(device, wifi);

            return Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(16),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      Icon(Icons.wifi,
                          color: isConnectedToWifi ? Colors.blue : Colors.grey),
                      Padding(padding: const EdgeInsets.all(8)),
                      Expanded(
                          child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Text(
                            wifi.name,
                            style: TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          // display connection status
                          _trueOrFalseText(
                            isConnectedToWifi
                                ? _locale.connected
                                : _locale.notConnected,
                            isConnectedToWifi,
                          ),
                          Padding(padding: EdgeInsets.all(8)),
                          // display distance from wifi point and device
                          Text(_locale.distanceValue(totalDistance)),
                          // display if device inside wifi radius
                          _trueOrFalseText(
                            _locale.insideWifiMeterRadius(isInsideRadius),
                            isInsideRadius,
                          ),
                          // display geofence status, whether it fulfill the
                          // requirements or not
                          _trueOrFalseText(
                              _locale.geofenceStatus(isUnderWifiCoverage),
                              isUnderWifiCoverage),
                        ],
                      )),
                      Icon(Icons.navigate_next, color: Colors.black26),
                    ],
                  ),
                ),
                Divider(),
              ],
            );
          }

          Widget _listOfWifi() => Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: EdgeInsets.all(16),
                    child: Text(_locale.tapToConnect),
                  ),
                  Expanded(
                    child: ListView(
                        children: state.wifiList.entries.map((wifiMap) {
                      return InkWell(
                        onLongPress: () =>
                            _navigateToWifiSettings(wifiMap.value),
                        onTap: () => _setAsActiveWifi(wifiMap.value),
                        child: _cardWifi(wifiMap.value),
                      );
                    }).toList()),
                  ),
                ],
              );

          // if there is no wifi available,
          // ask to setup first wifim
          // else show list of available wifi
          Widget _listOfWifiCard() {
            if (state.wifiList.isEmpty) {
              return Center(
                  child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Text(_locale.noWifiDetected),
                  _buttonNavigateToWifiSetup(),
                ],
              ));
            } else {
              return Stack(
                children: <Widget>[
                  _listOfWifi(),
                  Positioned(
                    bottom: 16,
                    right: 16,
                    child: _buttonNavigateToWifiSetup(),
                  ),
                ],
              );
            }
          }

          return Container(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(16),
                  child: _displayDeviceInformation(),
                ),
                Expanded(
                  child: SafeArea(
                    child: Card(
                      margin: EdgeInsets.all(16),
                      elevation: 20,
                      shadowColor: Colors.grey[50],
                      color: Colors.white,
                      child: Container(
                        child: _listOfWifiCard(),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          );
        },
      ),
    );
  }
}

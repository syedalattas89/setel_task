import 'package:flutter_test/flutter_test.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:setel_task/models/device.dart';
import 'package:setel_task/models/wifi.dart';
import 'package:setel_task/utils/util.dart';

void main() {
  LatLng locationStart = LatLng(5.057259, -1.906502);
  LatLng locationEnd = LatLng(5.058241, -1.902340);
  LatLng locationEndFar = LatLng(5.166045, -1.675085);

  test('distance should return correctly in KM', () async {
    final double distance = calculateDistance(
      locationStart,
      locationEnd,
    );
    expect(distance, 0.47);
    // verified using this calculator online
    // https://www.geodatasource.com/distance-calculator
  });

  test('if device is within wifi network radius, return true', () {
    Wifi wifi = Wifi(
      id: 'some_id',
      name: 'test_wifi',
      location: locationEnd,
      radius: 10,
    );
    Device device = Device(wifi: wifi.name, location: locationStart);

    // 0.43 < 10 = true
    final bool isWithinRadius = isDeviceInsideWifiRadius(device, wifi);
    expectLater(isWithinRadius, true);
  });

  test('if device is outside wifi network radius, return false', () {
    Wifi wifi = Wifi(
      id: 'some_id',
      name: 'test_wifi',
      location: locationEndFar,
      radius: 10,
    );
    Device device = Device(wifi: wifi.name, location: locationStart);

    // 28.34 < 10 = false
    final bool isWithinRadius = isDeviceInsideWifiRadius(device, wifi);
    expectLater(isWithinRadius, false);
  });

  test('if device is connected to the same wifi, return true', () {
    Wifi wifi = Wifi(
      id: 'some_id',
      name: 'test_wifi',
      location: locationEndFar,
      radius: 10,
    );
    Device device = Device(wifi: wifi.name, location: locationStart);

    final bool isConnected = isDeviceConnectedToWifi(device, wifi);
    expect(isConnected, true);
  });

  test('if device is not connected to the same wifi, return false', () {
    Wifi wifi = Wifi(
      id: 'some_id',
      name: 'test_wifi',
      location: locationEndFar,
      radius: 10,
    );
    Device device = Device(wifi: 'different_wifi', location: locationStart);

    final bool isConnected = isDeviceConnectedToWifi(device, wifi);
    expect(isConnected, false);
  });

  test('if device is under wifi coverage, return true', () {
    Wifi wifi = Wifi(
      id: 'some_id',
      name: 'test_wifi',
      location: locationEndFar,
      radius: 10,
    );
    Device device = Device(wifi: wifi.name, location: locationStart);

    final bool isUnderCoverage = isDeviceUnderWifiCoverage(device, wifi);
    expect(isUnderCoverage, false);
  });

  test('if device is not under wifi coverage, return false', () {
    Wifi wifi = Wifi(
      id: 'some_id',
      name: 'test_wifi',
      location: locationEndFar,
      radius: 10,
    );
    Device device = Device(wifi: 'different_wifi', location: locationStart);

    final bool isUnderCoverage = isDeviceUnderWifiCoverage(device, wifi);
    expect(isUnderCoverage, false);
  });

  test('if device or wifi null, return false', () {
    Wifi wifi = Wifi(
      id: 'some_id',
      name: 'test_wifi',
      location: null,
      radius: 10,
    );
    Device device = Device(wifi: 'different_wifi', location: null);

    final bool isUnderCoverage = isDeviceUnderWifiCoverage(device, wifi);
    expect(isUnderCoverage, false);
  });
}

# setel_task

To create a Flutter application that will detect if the device is located inside of a geofence area.

# requirements
[Requirements are provided from here](https://docs.google.com/document/d/1pkxwhGSmvDei3BRm9Ix7tVUGose_Lpp-MBJzvO1T31w/edit#)

# apk
[Link to download](https://drive.google.com/file/d/1cN8jM95JHGEuIw9cgJ0_C5U0i2j39znI/view?usp=sharing)

Feature summary from this build:
<p>1. Display basic information and geofence status <br\>
   a. Tap any WiFi in the list to 'connect' <br\>
   b. Geofence and connection status be updated in the list <br\></p>
<p>2. Add and edit WiFi<br\>
   a. Tap Add WiFi button from home screen to add new WiFi<br\>
   b. Hold any WiFi in the existing list to 'edit'<br\>
   c. Name WiFi<br\>
   d. Adjust WiFi location by dragging the marker<br\>
   e. Red marker to represent WiFi (draggable)<br\>
   f. Blue marker to represent device location, <br\>
      to make it easier to see if wifi cover device location (cannot be dragged)<br\></p>

# architecture
[Roughly based on Flutter BLoC](https://pub.dev/packages/flutter_bloc)


# Useful Links

[Website to get latitude longitude](https://www.latlong.net/)

[Online calculator to calculate distance between two geo points](https://www.geodatasource.com/distance-calculator)


# screens

Home screen - no WiFi in the list

Permission will be asked before user can continue to 'Add Wifi'

![Home No Wifi](assets/screen_home_no_wifi.png)



Home screen - WiFi in the list showing the status before/after connected

![Home With Wifi](assets/screen_home_yes_wifi.png)



Set WiFi screen - WiFi in the list showing the status before/after connected

![Setup WiFi](assets/screen_set_wifi.png)


## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
